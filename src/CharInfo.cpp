#include "CharInfo.h"

CharInfo::CharInfo(): charTarget(nullptr)
{
    background.setTexture(Configuration::textures.get(Configuration::EXIBITION));
    
    team.setFont(Configuration::fonts.get(Configuration::INFO));
    team.setColor(sf::Color::Black);
    grade.setFont(Configuration::fonts.get(Configuration::INFO));
    grade.setColor(sf::Color::Black);
    life.setFont(Configuration::fonts.get(Configuration::INFO));
    life.setColor(sf::Color::Black);
    attack.setFont(Configuration::fonts.get(Configuration::INFO));
    attack.setColor(sf::Color::Black);
}

CharInfo::~CharInfo(){ /*emtpy*/ }

void CharInfo::setPosition(sf::Vector2i _pos)
{
    position = _pos;
    background.setPosition(position.x, position.y);
}

void CharInfo::setCharInfo(Character * _char)
{   
    charTarget = _char;
    if(charTarget)
    {
        if(_char->getTeam() == Character::BLUE)
        {
            team.setString("TIME: Azul");
            if(_char->getGrade() == Character::SWORDSMAN)
            {
                face.setTexture(Configuration::textures.get(Configuration::FACE_SWORDSMAN_BLUE));
                grade.setString("CLASSE: Espadachim");
            }                
            else if(_char->getGrade() == Character::ARCHER)
            {
                face.setTexture(Configuration::textures.get(Configuration::FACE_ARCHER_BLUE));
                grade.setString("CLASSE: Arqueiro"); 
            }            
        }
        else if(_char->getTeam() == Character::RED)
        {
            team.setString("TIME: Vermelho");
            if(_char->getGrade() == Character::SWORDSMAN)
            {
                face.setTexture(Configuration::textures.get(Configuration::FACE_SWORDSMAN_RED));
                grade.setString("CLASSE: Espadachim");
            }                
            else if(_char->getGrade() == Character::ARCHER)
            {
                face.setTexture(Configuration::textures.get(Configuration::FACE_ARCHER_RED));
                grade.setString("CLASSE: Arqueiro"); 
            }            
        }
        
        std::stringstream temp;
        
        temp << "VIDA: " << _char->getLife();
        life.setString(temp.str());
        temp.str(std::string());
        
        temp << "ATAQUE: " << _char->getAttack();
        attack.setString(temp.str());

        face.setPosition(position.x + 10, position.y + 30);
        team.setCharacterSize(15);
        team .setPosition(position.x + 120, position.y + 30);
        grade.setCharacterSize(15);
        grade .setPosition(position.x + 120, position.y + 65);
        life.setCharacterSize(15);
        life .setPosition(position.x + 120, position.y + 100);
        attack.setCharacterSize(15);
        attack .setPosition(position.x + 120, position.y + 135);
    }    
}

void CharInfo::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    target.draw(background, states);
    if( charTarget)
    {
        target.draw(face, states);
        target.draw(team, states);
        target.draw(grade, states);
        target.draw(life, states);
        target.draw(attack, states);
    }   
}

