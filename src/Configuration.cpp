#include <Configuration.h>

ResourceManager<sf::Texture, int> Configuration::textures;
ResourceManager<sf::Font, int> Configuration::fonts;
Configuration::Map Configuration::map;
std::unordered_map <int, Animation> Configuration::animations;

void Configuration::initialize()
{
    initTextures();
    initAnimations();
    initFonts();
    initMap();
}

void Configuration::initTextures()
{
    textures.load(Textures::BLUE_ARCHER, "media/sprites/blue_archer.png");
    textures.load(Textures::RED_ARCHER, "media/sprites/red_archer.png");
    textures.load(Textures::BLUE_SWORDSMAN, "media/sprites/blue_swordsman.png");
    textures.load(Textures::RED_SWORDSMAN, "media/sprites/red_swordsman.png");
    textures.load(Textures::BOARD, "media/sprites/board.png");
    textures.load(Textures::MOVE, "media/sprites/move_button.png");
    textures.load(Textures::ATTACK, "media/sprites/attack_button.png");
    textures.load(Textures::END, "media/sprites/end_button.png");
    textures.load(Textures::MENU, "media/sprites/menu_button.png");
    textures.load(Textures::EXIBITION, "media/sprites/exibition.png");
    textures.load(Textures::FACE_SWORDSMAN_BLUE, "media/sprites/swordsman_face_blue.png");
    textures.load(Textures::FACE_SWORDSMAN_RED, "media/sprites/swordsman_face_red.png");
    textures.load(Textures::FACE_ARCHER_BLUE, "media/sprites/archer_face_blue.png");
    textures.load(Textures::FACE_ARCHER_RED, "media/sprites/archer_face_red.png");
    textures.load(Textures::MAIN_MENU_BACKGROUND, "media/sprites/main_menu_background.png");
    textures.load(Textures::NEW_GAME, "media/sprites/new_game_button.png");
    textures.load(Textures::EXIT, "media/sprites/exit_button.png");
    textures.load(Textures::GAME_OVER_BACKGROUND, "media/sprites/game_over_background.png");
}

void Configuration::initAnimations()
{
    //Arqueiro do time Azul
    Animation blue_archer_move_down(&textures.get(BLUE_ARCHER));
    blue_archer_move_down.addFramesLine(3,4,0);
    animations.emplace(BLUE_ARCHER_DOWN, blue_archer_move_down);
    
    Animation blue_archer_move_lef(&textures.get(BLUE_ARCHER));
    blue_archer_move_lef.addFramesLine(3,4,1);
    animations.emplace(BLUE_ARCHER_LEFT, blue_archer_move_lef);
    
    Animation blue_archer_move_right(&textures.get(BLUE_ARCHER));
    blue_archer_move_right.addFramesLine(3,4,2);
    animations.emplace(BLUE_ARCHER_RIGHT, blue_archer_move_right);
    
    Animation blue_archer_move_up (&textures.get(BLUE_ARCHER));
    blue_archer_move_up.addFramesLine(3,4,3);
    animations.emplace(BLUE_ARCHER_UP, blue_archer_move_up);
    
    //Arqueiro do time vermelho
    Animation red_archer_move_down(&textures.get(RED_ARCHER));
    red_archer_move_down.addFramesLine(3,4,0);
    animations.emplace(RED_ARCHER_DOWN, red_archer_move_down);
    
    Animation red_archer_move_lef(&textures.get(RED_ARCHER));
    red_archer_move_lef.addFramesLine(3,4,1);
    animations.emplace(RED_ARCHER_LEFT, red_archer_move_lef);
    
    Animation red_archer_move_right(&textures.get(RED_ARCHER));
    red_archer_move_right.addFramesLine(3,4,2);
    animations.emplace(RED_ARCHER_RIGHT, red_archer_move_right);
    
    Animation red_archer_move_up (&textures.get(RED_ARCHER));
    red_archer_move_up.addFramesLine(3,4,3);
    animations.emplace(RED_ARCHER_UP, red_archer_move_up);
    
    //Espadachim do time azul
    Animation blue_swordsman_move_down(&textures.get(BLUE_SWORDSMAN));
    blue_swordsman_move_down.addFramesLine(3,4,0);
    animations.emplace(BLUE_SWORDSMAN_DOWN, blue_swordsman_move_down);
    
    Animation blue_swordsman_move_lef(&textures.get(BLUE_SWORDSMAN));
    blue_swordsman_move_lef.addFramesLine(3,4,1);
    animations.emplace(BLUE_SWORDSMAN_LEFT, blue_swordsman_move_lef);
    
    Animation blue_swordsman_move_right(&textures.get(BLUE_SWORDSMAN));
    blue_swordsman_move_right.addFramesLine(3,4,2);
    animations.emplace(BLUE_SWORDSMAN_RIGHT, blue_swordsman_move_right);
    
    Animation blue_swordsman_move_up (&textures.get(BLUE_SWORDSMAN));
    blue_swordsman_move_up.addFramesLine(3,4,3);
    animations.emplace(BLUE_SWORDSMAN_UP, blue_swordsman_move_up);
    
    //Espadachim do time vermelho    
    Animation red_swordsman_move_down(&textures.get(RED_SWORDSMAN));
    red_swordsman_move_down.addFramesLine(3,4,0);
    animations.emplace(RED_SWORDSMAN_DOWN, red_swordsman_move_down);
    
    Animation red_swordsman_move_lef(&textures.get(RED_SWORDSMAN));
    red_swordsman_move_lef.addFramesLine(3,4,1);
    animations.emplace(RED_SWORDSMAN_LEFT, red_swordsman_move_lef);
    
    Animation red_swordsman_move_right(&textures.get(RED_SWORDSMAN));
    red_swordsman_move_right.addFramesLine(3,4,2);
    animations.emplace(RED_SWORDSMAN_RIGHT, red_swordsman_move_right);
    
    Animation red_swordsman_move_up (&textures.get(RED_SWORDSMAN));
    red_swordsman_move_up.addFramesLine(3,4,3);
    animations.emplace(RED_SWORDSMAN_UP, red_swordsman_move_up);
}

void Configuration::initFonts()
{
    fonts.load(Fonts::INFO, "media/fonts/ATypewriterForMe.ttf");
}

void Configuration::initMap()
{
    std::ifstream myFile("media/maps/map01.txt");
    if (!myFile)
        throw std::runtime_error("Impossible to load file");

    std::string line;
    getline(myFile, line);
    std::istringstream nLine(line);

    nLine >> map.size.x;
    nLine >> map.size.y;

    char c;

    map.cell.resize(map.size.x);
    for (int i = 0; i < map.size.x; ++i)
    {
        getline(myFile, line);
        std::istringstream nLine(line);
        for (int j = 0; j < map.size.y; ++j)
        {
            nLine >> c;
            int n = c - '0';
            map.cell[i].push_back(n);
        }
    }

    myFile.close();

}

