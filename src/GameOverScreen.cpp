#include <GameOverScreen.h>

GameOverScreen::GameOverScreen() : nextScreen(3)
{
    background.setTexture(Configuration::textures.get(Configuration::GAME_OVER_BACKGROUND));
    background.setPosition(0,0);
    
    menuButton.setTexture(Configuration::textures.get(Configuration::MENU));
    menuButton.setPosition(300,250);
    
    exitButton.setTexture(Configuration::textures.get(Configuration::EXIT));
    exitButton.setPosition(300,350);
    
    winnerText.setFont(Configuration::fonts.get(Configuration::INFO));
    winnerText.setPosition(100,150);
    winnerText.setColor(sf::Color::White);
    winnerText.setCharacterSize(50);
}

GameOverScreen::~GameOverScreen(){ }

int GameOverScreen::getnextScreen()
{
    return nextScreen;
}

void GameOverScreen::setWinner(std::string _winner)
{
    this->winner = _winner;
}



void GameOverScreen::run()
{
    window.create(sf::VideoMode(800, 600), "TATICA");
    winnerText.setString("Vencedor: " + winner);
    
    while(window.isOpen())
    {
        sf::Event event;
        
        while(window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
            {
                nextScreen = 0;
                window.close();
            } 
            else if (event.type == sf::Event::KeyPressed)
            {
                if (event.key.code == sf::Keyboard::Escape)
                {
                    nextScreen = 0;
                    window.close();
                }
            }
            else if(event.type == sf::Event::MouseButtonPressed)
            {
                if (event.mouseButton.button == sf::Mouse::Left)
                {
                    sf::Vector2f pos(event.mouseButton.x, event.mouseButton.y);

                    if (menuButton.getGlobalBounds().contains(pos))
                    {
                        nextScreen = 1;
                        window.close();
                    } 
                    else if (exitButton.getGlobalBounds().contains(pos))
                    {
                        nextScreen = 0;
                        window.close();
                    } 
                }
            }
        }
        
        window.clear();
        
        window.draw(background);
        window.draw(menuButton);
        window.draw(exitButton);
        window.draw(winnerText);
        
        window.display();
    }
}

