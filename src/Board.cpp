#include <Board.h>

Board::Board(sf::Vector2i _size, int _cellSize, int numChar) : size(_size), cellSize(_cellSize), state(WAITING), moved(false), attacked(false), endGame(false)
{
    mBoard = new Cell * [size.x];
    for (int i = 0; i < size.x; i++)
    {
        mBoard[i] = new Cell[size.y];
    }

    bound.height = size.x * cellSize;
    bound.width = size.y * cellSize;
    bound.left = 0;
    bound.top = 0;

    for (int i = 0; i < size.x; i++)
    {
        for (int j = 0; j < size.y; j++)
        {
            mBoard[i][j].cellSprite.setPosition(j * cellSize, i * cellSize);
            mBoard[i][j].cellSprite.setTexture(Configuration::textures.get(Configuration::BOARD));
            int temp = Configuration::map.cell[i][j];
            mBoard[i][j].cellSprite.setTextureRect(sf::IntRect(temp * cellSize, 0, cellSize, cellSize));
        }
    }

    int begin = (size.x - numChar) /2;
    for (int i = begin; i < begin + numChar; i++)
    {
        mBoard[i][5].resident = new Character(Character::ARCHER, Character::BLUE, cellSize, sf::Vector2i(i, 5));
        charPosition.push(sf::Vector2i(i,5));
        //std::cout << "Adicionado: [" << i<< ", " << 5 << "]" << std::endl;

        mBoard[i][size.y - 6].resident = new Character(Character::ARCHER, Character::RED, cellSize, sf::Vector2i(i, size.y - 6));
        charPosition.push(sf::Vector2i(i, size.y - 6));
        //std::cout << "Adicionado: [" << i<< ", " << size.y - 6 << "]" << std::endl;

        mBoard[i][6].resident = new Character(Character::SWORDSMAN, Character::BLUE, cellSize, sf::Vector2i(i, 6));
        charPosition.push(sf::Vector2i(i, 6));
        //std::cout << "Adicionado: [" << i<< ", " << 6 << "]" << std::endl;

        mBoard[i][size.y - 7].resident = new Character(Character::SWORDSMAN, Character::RED, cellSize, sf::Vector2i(i, size.y - 7));
        charPosition.push(sf::Vector2i(i, size.y - 7));
        //std::cout << "Adicionado: [" << i<< ", " << size.y - 7 << "]" << std::endl;

    }    
    infoCurrent.setPosition(sf::Vector2i(0,bound.height));
    infoCurrent.setCharInfo(mBoard[charPosition.getDataTurn().x][charPosition.getDataTurn().y].resident);
    
    currentCharPosition.x = charPosition.getDataTurn().x;
    currentCharPosition.y = charPosition.getDataTurn().y;
    
    infoTarget.setPosition(sf::Vector2i(500, bound.height));
}

Board::~Board()
{
    for (int i = 0; i < size.x; i++)
    {
        delete[] mBoard[i];
    }
    delete[] mBoard;

}

void Board::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    for (int i = 0; i < size.x; i++)
    {
        for (int j = 0; j < size.y; j++)
        {
            target.draw(mBoard[i][j], states);
        }
    }
    target.draw(infoCurrent, states);
    target.draw(infoTarget, states);
    target.draw(mBoard[currentCharPosition.x][currentCharPosition.y], states);
}

sf::Rect<float> Board::getBound()
{
    return this->bound;
}

void Board::moveAction()
{
    if(!moved && state == WAITING)
    {
        //Mostrar Células possíveis para movimento
        sf::Vector2i pos = charPosition.getDataTurn();
        int speed = mBoard[pos.x][pos.y].resident->getSpeed();

        for (int i = 0; i < size.x; i++)
        {
            for (int j = 0; j < size.y; j++)
            {
                if((abs(pos.x - i) + abs(pos.y - j)) <= speed && mBoard[i][j].resident == nullptr)
                {
                    mBoard[i][j].cellSprite.setColor(sf::Color::Cyan);
                }
            }
        }
        state = READY_TO_MOVE;
    }
}

void Board::attackAction()
{
    if(!attacked && state == WAITING)
    {
        //Mostrar Células possíveis para ataque
        sf::Vector2i pos = charPosition.getDataTurn();
        Character::team team = mBoard[pos.x][pos.y].resident->getTeam();
        int range = mBoard[pos.x][pos.y].resident->getRange();

        for (int i = 0; i < size.x; i++)
        {
            for (int j = 0; j < size.y; j++)
            {
                if((abs(pos.x - i) + abs(pos.y - j)) <= range)
                {
                    if(mBoard[i][j].resident == nullptr)
                        mBoard[i][j].cellSprite.setColor(sf::Color::Yellow);
                    else if(mBoard[i][j].resident->getTeam() != team)
                        mBoard[i][j].cellSprite.setColor(sf::Color::Red);
                }
            }
        }
        state = READY_TO_ATTACK;
    }
}

void Board::endAction()
{
    if(state == WAITING)
    {        
        charPosition.run();
        currentCharPosition.x = charPosition.getDataTurn().x;
        currentCharPosition.y = charPosition.getDataTurn().y;
        moved = false;
        attacked = false;
        infoCurrent.setCharInfo(mBoard[charPosition.getDataTurn().x][charPosition.getDataTurn().y].resident);
    }
}

void Board::cellAction(sf::Vector2i _pos)
{
    int x = floor(_pos.y / cellSize);
    int y = floor(_pos.x / cellSize);
    selectedCellPosition = sf::Vector2i(x, y);
    //std::cout << "Pos: [" << x << ", " << y << "]" << std::endl;

    if(state == WAITING)
    {
        //Mostrar informações da célula
        infoTarget.setCharInfo(mBoard[x][y].resident);
    }
    else if(state == READY_TO_MOVE)
    {
        if (mBoard[x][y].cellSprite.getColor() == sf::Color::Cyan)
        {
            clear();
            moved = true;
            state = MOVING; 
        }
    }  
    else if(state == READY_TO_ATTACK)
    {
        if (mBoard[x][y].cellSprite.getColor() == sf::Color::Red)
        {
            attack(*mBoard[charPosition.getDataTurn().x][charPosition.getDataTurn().y].resident, *mBoard[x][y].resident);

            if(mBoard[x][y].resident->getLife() == 0)
            {
               charPosition.remove(selectedCellPosition);
               //std::cout << "Removendo: [" << selectedCellPosition.x << ", " << selectedCellPosition.y << "]" << std::endl;
               delete mBoard[x][y].resident;
               mBoard[x][y].resident = nullptr;
            }

            testEndGame();

            clear();
            state = WAITING;
            attacked = true;
            
            infoTarget.setCharInfo(mBoard[x][y].resident);
        }
    }
}

void Board::cancelAction()
{
    if(state == READY_TO_MOVE || state == READY_TO_ATTACK)
    {
        clear();
        state = WAITING;
    }
}

void Board::clear()
{
    for (int i = 0; i < size.x; i++)
    {
        for (int j = 0; j < size.y; j++)
        {
           mBoard[i][j].cellSprite.setColor(sf::Color::White);
        }
    }
}

void Board::attack(Character& _char, Character& _target)
{
    _target.decreaseLife(_char.getAttack());
}
void Board::update(sf::Time time)
{
    Character * current = mBoard[charPosition.getDataTurn().x][charPosition.getDataTurn().y].resident;
    if(state == MOVING)
    {
        int right;
        int left;
        int down;
        int up;
        
        if(current->getTeam() == Character::BLUE && current->getGrade() == Character::ARCHER)
        {
            right = Configuration::BLUE_ARCHER_RIGHT;
            left = Configuration::BLUE_ARCHER_LEFT;
            down = Configuration::BLUE_ARCHER_DOWN;
            up = Configuration::BLUE_ARCHER_UP; 
        }
        else if(current->getTeam() == Character::BLUE && current->getGrade() == Character::SWORDSMAN)
        {
            right = Configuration::BLUE_SWORDSMAN_RIGHT;
            left = Configuration::BLUE_SWORDSMAN_LEFT;
            down = Configuration::BLUE_SWORDSMAN_DOWN;
            up = Configuration::BLUE_SWORDSMAN_UP; 
        }
        else if(current->getTeam() == Character::RED && current->getGrade() == Character::ARCHER)
        {
            right = Configuration::RED_ARCHER_RIGHT;
            left = Configuration::RED_ARCHER_LEFT;
            down = Configuration::RED_ARCHER_DOWN;
            up = Configuration::RED_ARCHER_UP; 
        }
        else if(current->getTeam() == Character::RED && current->getGrade() == Character::SWORDSMAN)
        {
            right = Configuration::RED_SWORDSMAN_RIGHT;
            left = Configuration::RED_SWORDSMAN_LEFT;
            down = Configuration::RED_SWORDSMAN_DOWN;
            up = Configuration::RED_SWORDSMAN_UP;
        }
        
        int x = selectedCellPosition.y * cellSize;
        int y = selectedCellPosition.x * cellSize;
        
        if(floor(current->getAnimatedSprite().getPosition().y) < y)
        {
            current->getAnimatedSprite().setAnimation(&Configuration::animations.at(down));
            current->getAnimatedSprite().move(0,0.3);
        }
        else if(floor(current->getAnimatedSprite().getPosition().y) > y)
        {
            current->getAnimatedSprite().setAnimation(&Configuration::animations.at(up));
            current->getAnimatedSprite().move(0,-0.3);
        }
        else if(floor(current->getAnimatedSprite().getPosition().x) < x)
        {
            current->getAnimatedSprite().setAnimation(&Configuration::animations.at(right));
            current->getAnimatedSprite().move(0.3,0);
        }
        else if(floor(current->getAnimatedSprite().getPosition().x) > x)
        {
            current->getAnimatedSprite().setAnimation(&Configuration::animations.at(left));
            current->getAnimatedSprite().move(-0.3,0);
        }
        else
        {
            mBoard[selectedCellPosition.x][selectedCellPosition.y].resident = current;
            mBoard[charPosition.getDataTurn().x][charPosition.getDataTurn().y].resident = nullptr;
            charPosition.setDataTurn(sf::Vector2i(selectedCellPosition.x, selectedCellPosition.y));
            state = WAITING;
            //std::cout << "Movendo: [" << selectedCellPosition.x << ", " << selectedCellPosition.y << "]" << std::endl;
            //std::cout << "Turno Atual: [" << charPosition.getDataTurn().x << ", " << charPosition.getDataTurn().y << "]" << std::endl;            
        } 
    }
    
    for (int i = 0; i < size.x; i++)
    {
        for (int j = 0; j < size.y; j++)
        {
            if(mBoard[i][j].resident)
                mBoard[i][j].resident->update(time);
        }
    }
}

void Board::testEndGame()
{
    int count_blue = 0;
    int count_red = 0;
    for (int i = 0; i < size.x; i++)
    {
        for (int j = 0; j < size.y; j++)
        {
            if(mBoard[i][j].resident)
            {
                if(mBoard[i][j].resident->getTeam() == Character::BLUE)
                    count_blue ++;
                else if(mBoard[i][j].resident->getTeam() == Character::RED)
                    count_red ++;
            }   
        }
    }
    
    
    if(count_red == 0)
    {
        winner = "Time Azul";
        endGame = true;
    }        
    else if (count_blue == 0)
    {
        winner = "Time Vermelho";
        endGame = true;
    }      
}

bool Board::getEndGame()
{
    return endGame;
}

std::string Board::getWinner()
{
    return this->winner;
}


