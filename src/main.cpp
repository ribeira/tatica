#include <Game.h>
#include <MainScreenGame.h>
#include <GameOverScreen.h>

int main()
{
    Configuration::initialize();
    MainScreenGame mainScreenGame;
    GameOverScreen gameOverScreen;
    Game game;
    
    int screen = 1;
    
    while(screen > 0)
    {
        if(screen == 1)
        {
            mainScreenGame.run();
            screen = mainScreenGame.getnextScreen();
        }
        else if(screen == 2)
        {
            game.run(30);
            screen = game.getNextScreen();
        }
        else if(screen == 3)
        {
            gameOverScreen.setWinner(game.getWinner());
            gameOverScreen.run();
            screen = gameOverScreen.getnextScreen();
        }
    }
    
    return EXIT_SUCCESS;
}