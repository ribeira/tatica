#include <MainScreenGame.h>

MainScreenGame::MainScreenGame() : nextScreen(1)
{
    background.setTexture(Configuration::textures.get(Configuration::MAIN_MENU_BACKGROUND));
    background.setPosition(0,0);
    
    newGameButton.setTexture(Configuration::textures.get(Configuration::NEW_GAME));
    newGameButton.setPosition(300,250);
    
    exitButton.setTexture(Configuration::textures.get(Configuration::EXIT));
    exitButton.setPosition(300,350);
}

MainScreenGame::~MainScreenGame(){ }

int MainScreenGame::getnextScreen()
{
    return nextScreen;
}


void MainScreenGame::run()
{
    window.create(sf::VideoMode(800, 600), "TATICA");
    while(window.isOpen())
    {
        sf::Event event;
        
        while(window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
            {
                nextScreen = 0;
                window.close();
            } 
            else if (event.type == sf::Event::KeyPressed)
            {
                if (event.key.code == sf::Keyboard::Escape)
                {
                    nextScreen = 0;
                    window.close();
                }
            }
            else if(event.type == sf::Event::MouseButtonPressed)
            {
                if (event.mouseButton.button == sf::Mouse::Left)
                {
                    sf::Vector2f pos(event.mouseButton.x, event.mouseButton.y);

                    if (newGameButton.getGlobalBounds().contains(pos))
                    {
                        nextScreen = 2;
                        window.close();
                    } 
                    else if (exitButton.getGlobalBounds().contains(pos))
                    {
                        nextScreen = 0;
                        window.close();
                    } 
                }
            }
        }
        
        window.clear();
        
        window.draw(background);
        window.draw(newGameButton);
        window.draw(exitButton);
        
        window.display();
    }
}


