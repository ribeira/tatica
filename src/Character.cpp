#include <iostream>

#include "Character.h"
#include "CharInfo.h"

Character::Character(grade _gr, team _tm, int _cellSize, sf::Vector2i _pos) : myTeam(_tm), myGrade(_gr), position(_pos)
{
    
   
    if(myGrade == ARCHER)
    {
        attack = 40;
        life = 80;
        speed = 3;
        range = 4;
        
        if(myTeam == BLUE)
            charSprite.setAnimation(&Configuration::animations.at(Configuration::BLUE_ARCHER_RIGHT));
        else
            charSprite.setAnimation(&Configuration::animations.at(Configuration::RED_ARCHER_LEFT));
    }
    else if(myGrade == SWORDSMAN)
    {
        attack = 20;
        life = 120;
        speed = 6;
        range = 1;
        
        if(myTeam == BLUE)
            charSprite.setAnimation(&Configuration::animations.at(Configuration::BLUE_SWORDSMAN_RIGHT));
        else
            charSprite.setAnimation(&Configuration::animations.at(Configuration::RED_SWORDSMAN_LEFT));
    }

    charSprite.setPosition(position.y * _cellSize, position.x * _cellSize);
    charSprite.setFrameTime(sf::seconds(0.2));
}

Character::~Character(){ /*Empty*/ }


AnimatedSprite& Character::getAnimatedSprite()
{
    return this->charSprite;
}

Character::team Character::getTeam()
{
    return this->myTeam;
}

Character::grade Character::getGrade()
{
    return this->myGrade;
}

int Character::getAttack()
{
    return this->attack;
}

int Character::getLife()
{
    return this->life;
}

int Character::getSpeed()
{
    return this->speed;
}

int Character::getRange()
{
    return this->range;
}

void Character::decreaseLife(int damage)
{
    if(damage > life)
        life = 0;
    else
        life -= damage;
}

void Character::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    target.draw(charSprite, states);
}

void Character::update(sf::Time time)
{
    this->getAnimatedSprite().play();
    this->getAnimatedSprite().update(time);
}

