#include "Cell.h"

void Cell::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    target.draw(cellSprite, states);
    if (resident != nullptr)
    {
        target.draw(*resident, states);
    }
}

