#include <Game.h>

Game::Game() : gameOver(false), nextScreen(2)
{
    board = new Board(sf::Vector2i(10, 20), 40, 1);
    buttons.push_back(sf::Sprite(Configuration::textures.get(Configuration::MOVE)));
    buttons.back().setPosition(300, 400);
    buttons.push_back(sf::Sprite(Configuration::textures.get(Configuration::ATTACK)));
    buttons.back().setPosition(300, 450);
    buttons.push_back(sf::Sprite(Configuration::textures.get(Configuration::END)));
    buttons.back().setPosition(300, 500);
    buttons.push_back(sf::Sprite(Configuration::textures.get(Configuration::MENU)));
    buttons.back().setPosition(300, 550);    
}

Game::~Game()
{
    delete board;
}

void Game::run(int minimum_frame_per_seconds)
{    
    restart();
    window.create(sf::VideoMode(800, 600), "TATICA");
    
    sf::Clock clock;
    sf::Time timeSinceLastUpdate;
    sf::Time timePerFrame = sf::seconds(1.f / minimum_frame_per_seconds);

    while (window.isOpen() && !gameOver)
    {
        processEvents();
        timeSinceLastUpdate = clock.restart();

        while (timeSinceLastUpdate > timePerFrame)
        {
            timeSinceLastUpdate -= timePerFrame;
            update(timePerFrame);
        }
        update(timeSinceLastUpdate);
        render();
    }
    
    if(gameOver)
    {
        winner = board->getWinner();
        nextScreen = 3;
        window.close();
    }

}

void Game::processEvents()
{
    sf::Event event;
    while (window.pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
        {
            nextScreen = 0;
            window.close();
        } 
        else if (event.type == sf::Event::KeyPressed)
        {
            if (event.key.code == sf::Keyboard::Escape)
            {
                nextScreen = 0;
                window.close();
            }
        }
        if (event.type == sf::Event::MouseButtonPressed)
        {
            if (event.mouseButton.button == sf::Mouse::Left)
            {
                sf::Vector2f pos(event.mouseButton.x, event.mouseButton.y);
                //std::cout << "Mouse: [" << pos.x << ", " << pos.y << " ]" << std::endl;

                if (buttons[ATTACK].getGlobalBounds().contains(pos))
                {
                    board->attackAction();
                } 
                else if (board->getBound().contains(pos))
                {
                    //Clicou em uma célula
                    board->cellAction(sf::Vector2i(pos.x, pos.y));
                } 
                else if (buttons[MOVE].getGlobalBounds().contains(pos))
                {
                    //Clicou em mover
                    board->moveAction();
                } 
                else if (buttons[END].getGlobalBounds().contains(pos))
                {
                    //Clicou em encerrar
                    board->endAction();
                }
                else if (buttons[MENU].getGlobalBounds().contains(pos))
                {
                    nextScreen = 1;
                    window.close();
                }              
            } 
            else if (event.mouseButton.button == sf::Mouse::Right)
            {
                //Clicou em Cancelar (Botão Direito)
                board->cancelAction();
            }
        }
    }
}

void Game::update(sf::Time time) {
    //std::cout << "Entrou no update" << std::endl;
    gameOver = board->getEndGame();
    board->update(time);
}

void Game::render()
{
    window.clear();
    window.draw(*board);
    for (auto e : buttons)
    {
        window.draw(e);
    }
    window.display();
}

int Game::getNextScreen()
{
    return nextScreen;
}

std::string Game::getWinner()
{
    return this->winner;
}

void Game::restart()
{
    delete board;
    board = new Board(sf::Vector2i(10, 20), 40, 1);
    
    gameOver = false;
    nextScreen = 2;
}

