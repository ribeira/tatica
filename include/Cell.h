#ifndef CELL_H
#define CELL_H

#include <SFML/Graphics.hpp>
#include <Character.h>

class Cell : public sf::Drawable
{
    public:
        Character * resident;
        sf::Sprite cellSprite;

        Cell() : resident(nullptr)
        {
            /*emtpy*/
        }
        
        virtual ~Cell()
        {
            if (resident)
                delete resident;
        }


    private:
        void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};

#endif /* CELL_H */

