#ifndef CHARACTER_H
#define CHARACTER_H

#include <SFML/Graphics.hpp>
#include <Configuration.h>
#include <AnimatedSprite.h>

class Character : public sf::Drawable
{
    public:

        enum team {RED, BLUE};

        enum grade {ARCHER, SWORDSMAN};

    private:
        team myTeam;
        grade myGrade;
        sf::Vector2i position;
        int attack;
        int life;
        int speed;
        int range;
        AnimatedSprite charSprite;

        void draw(sf::RenderTarget& target, sf::RenderStates states) const;

    public:
        Character(grade _gr, team _tm, int cellSize, sf::Vector2i _pos);
        ~Character();
        AnimatedSprite& getAnimatedSprite();
        team getTeam();
        grade getGrade();
        int getAttack();
        int getLife();
        int getSpeed();
        int getRange();
        void decreaseLife(int damage);
        void update(sf::Time time);
};

#endif
