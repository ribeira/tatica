#ifndef GAME_H
#define GAME_H

#include <SFML/Graphics.hpp>
#include <Board.h>
#include <Character.h>
#include <ResourceManager.h>
#include <vector>

class Game
{
    public:

        enum Button {MOVE, ATTACK, END, MENU};
        Game(const Game&) = delete;
        Game& operator=(const Game&) = delete;
        Game();
        ~Game();
        void run(int minimum_frame_per_seconds);
        void restart();
        int getNextScreen();
        std::string getWinner();

    private:
        Board * board;
        std::vector<sf::Sprite> buttons;
        void processEvents();
        void update(sf::Time time);
        void render();
        
        sf::RenderWindow window;
        
        std::string winner;
        
        bool gameOver;
        int nextScreen;
};

#endif