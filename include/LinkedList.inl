#include <LinkedList.h>

template <typename Data>
LinkedList<Data>::LinkedList()
{

    this->Head = nullptr;
    this->End = nullptr;
    this->length = 0;
    this->turn = Head;
}

template <typename Data>
LinkedList<Data>::~LinkedList()
{
    clear();
}

template <typename Data>
void LinkedList<Data>::insert(const Data & _data)
{

    Node * tmp = new Node();
    tmp->data = _data;

    if (length == 0)
    {

        this->Head = tmp;
        this->End = tmp;
        this->turn = Head;
    } 
    else
    {

        tmp->next = Head;
        Head = tmp;

    }

    this->End->next = Head;
    tmp = nullptr;
    ++this->length;
}

template <typename Data>
unsigned int LinkedList<Data>::getLength()
{

    return this->length;

}

template <typename Data>
bool LinkedList<Data>::empty()
{
    return Head != nullptr ? false : true;
}

template <typename Data>
void LinkedList<Data>::push(const Data & _data)
{

    Node * tmp = new Node();
    tmp->data = _data;

    if (Head == nullptr)
    {

        Head = tmp;
        End = tmp;
        Head->next = End;
        this->turn = Head;

    } 
    else
    {

        End->next = tmp;
        End = tmp;

    }

    End->next = Head;
    tmp = nullptr;
    ++this->length;

}

template <typename Data>
bool LinkedList<Data>::remove(const Data & _data)
{

    Node * tmp = Head;
    Node * ant = End;
    bool test = true;

    do
    {
        if (tmp->data == _data)
        {
            test = false;
            break;
        }
        
        ant = tmp;
        tmp = tmp->next;
    }
    while (tmp != Head);
    

    if (test)
    {
        std::cout << "Valor não encontrado!" << std::endl;
    } 
    else
    {
        if (tmp == turn)
        {
            this->run();
        }
        
        if(tmp == End)
        {
            End = ant;
        }
        
        if (tmp == Head)
        {
            Head = tmp->next;
        }
        
        ant->next = tmp->next;      

        delete tmp;
        --this->length;
    }
    return test;
}

template <typename Data>
void LinkedList<Data>::clear()
{
    if(length > 0)
    {
        Node * tmp;

        while (Head != End)
        {
            tmp = Head;
            Head = Head->next;
            delete tmp;
        }
        
        delete Head;
        Head = nullptr;
        End = nullptr;
        turn = nullptr;
        length = 0;
    }
}

template <typename Data>
void LinkedList<Data>::run()
{
    this->turn = this->turn->next;
}

template <typename Data>
const Data LinkedList<Data>::getDataTurn()
{
    return this->turn->data;
}

template <typename Data>
void LinkedList<Data>::setDataTurn(const Data & _data)
{
    this->turn->data = _data;
}
