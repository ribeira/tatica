#ifndef GAMEOVERSCREEN_H
#define GAMEOVERSCREEN_H

#include <SFML/Graphics.hpp>
#include <Configuration.h>

class GameOverScreen
{
    public:
        GameOverScreen();
        ~GameOverScreen();
        int getnextScreen();
        void run();
        void setWinner(std::string _winner);

    private:
        sf::RenderWindow window;
        
        int nextScreen;     
                
        sf::Sprite background;
        sf::Sprite menuButton;
        sf::Sprite exitButton;
        std::string winner;
        sf::Text winnerText;

};

#endif /* GAMEOVERSCREEN_H */

