#ifndef BOARD_H
#define BOARD_H

#include <SFML/Graphics.hpp>
#include <Cell.h>
#include <Character.h>
#include <Configuration.h>
#include <CharInfo.h>
#include <LinkedList.h>
#include <iostream>

class Board : public sf::Drawable
{
    public:

        enum States : int { WAITING, READY_TO_MOVE, READY_TO_ATTACK, MOVING};
        Board(sf::Vector2i _size, int cellSize, int numChar);
        ~Board();

        sf::Rect<float> getBound();
        void attack(Character& _char, Character& _target);
        void moveAction();
        void attackAction();
        void endAction();
        void cellAction(sf::Vector2i _pos);
        void cancelAction();
        void clear();
        void update(sf::Time time);
        void testEndGame();
        bool getEndGame();
        std::string getWinner();

    private:
        void draw(sf::RenderTarget& target, sf::RenderStates states) const;
        sf::Vector2i size;
        int cellSize;
        LinkedList<sf::Vector2i> charPosition;
        Cell ** mBoard;
        CharInfo infoCurrent;
        CharInfo infoTarget;
        sf::Rect<float> bound;
        States state;
        bool moved;
        bool attacked;
        sf::Vector2i selectedCellPosition;
        sf::Vector2i currentCharPosition;
        std::string winner;
        bool endGame;
};

#endif
