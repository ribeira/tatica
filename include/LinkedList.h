#ifndef _LINKEDLIST_H_
#define _LINKEDLIST_H_

#include <iostream>

template <typename Data>

class LinkedList
{
    protected:

        LinkedList(const LinkedList &){}

        struct Node
        {
            Data data;
            Node * next;
        };

        unsigned int length;
        Node * Head;
        Node * End;
        Node * turn;

    public:
        //Construtor
        LinkedList();
        //Destructor
        ~LinkedList();
        //Retorna o tamanho da lista
        unsigned int getLength();
        //Insere no início da fila
        void insert(const Data & _data);
        //Insere no fim da fila
        void push(const Data & _data);
        //Remove de acordo com o dado
        bool remove(const Data & _data);
        //Limpa a fila
        void clear();
        //Checa se a fila está vazia
        bool empty();
        //Faz o apontador de turno andar
        void run();
        //Retorna o dado do nó do turno atual;
        const Data getDataTurn();
        //Modifica o dado do nó do turno atual;
        void setDataTurn(const Data & _data);

        inline friend std::ostream &operator<<(std::ostream& _os, const LinkedList& _oList)
        {
            Node * current = _oList.Head;

            if (_oList.Head == _oList.End)
            {
                _os << "The list is empty." << std::endl;
                return _os;
            }

            _os << "Fila: [ ";
            while (current->next != _oList.Head)
            {
                _os << "(" << current->data << ") ";
                current = current->next;
            }
            _os << "(" << current->data << ") " << "]" << std::endl;
            return _os;
        }
};

#include "LinkedList.inl"
#endif
