#ifndef CHARINFO_H
#define CHARINFO_H
#include <SFML/Graphics.hpp>
#include <Character.h>
#include <Configuration.h>
#include <sstream>


class CharInfo : public sf::Drawable
{
    public:
        CharInfo();
        ~CharInfo();

        void setPosition(sf::Vector2i);
        void setCharInfo(Character * _char);
       
    private:
        Character * charTarget;
        sf::Vector2i position;
        sf::Sprite background;
        sf::Sprite face;
        sf::Text team;
        sf::Text grade;
        sf::Text life;
        sf::Text attack;
        
        void draw(sf::RenderTarget& target, sf::RenderStates states) const;       
};

#endif /* CHARINFO_H */

