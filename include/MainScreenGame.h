#ifndef MAINSCREENGAME_H
#define MAINSCREENGAME_H

#include <SFML/Graphics.hpp>
#include <Configuration.h>

class MainScreenGame
{
    public:
        MainScreenGame();
        ~MainScreenGame();
        int getnextScreen();
        void run();

    private:
        sf::RenderWindow window;
        
        int nextScreen;     
                
        sf::Sprite background;
        sf::Sprite exitButton;
        sf::Sprite newGameButton;
};

#endif /* MAINSCREENGAME_H */

