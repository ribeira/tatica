#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <SFML/Graphics.hpp>
#include <ResourceManager.h>
#include <Character.h>
#include <Animation.h>
#include <fstream>
#include <string>
#include <vector>
#include <unordered_map>
#include <sstream>

class Configuration
{
    public:

        struct Map
        {
            sf::Vector2i size;
            std::vector< std::vector<int> > cell;
        };

        Configuration() = delete;
        Configuration(const Configuration&) = delete;
        Configuration& operator=(const Configuration&) = delete;

        enum Textures : int { BLUE_ARCHER, RED_ARCHER, BLUE_SWORDSMAN, RED_SWORDSMAN, BOARD, MOVE, ATTACK, END, MENU, EXIBITION,
                FACE_SWORDSMAN_BLUE, FACE_SWORDSMAN_RED, FACE_ARCHER_BLUE, FACE_ARCHER_RED, MAIN_MENU_BACKGROUND, EXIT, NEW_GAME, GAME_OVER_BACKGROUND};
        enum Animations: int
        { 
            BLUE_ARCHER_LEFT, BLUE_ARCHER_RIGHT, BLUE_ARCHER_DOWN, BLUE_ARCHER_UP,
            RED_ARCHER_LEFT, RED_ARCHER_RIGHT, RED_ARCHER_DOWN, RED_ARCHER_UP,
            BLUE_SWORDSMAN_LEFT, BLUE_SWORDSMAN_RIGHT, BLUE_SWORDSMAN_DOWN, BLUE_SWORDSMAN_UP,
            RED_SWORDSMAN_LEFT, RED_SWORDSMAN_RIGHT, RED_SWORDSMAN_DOWN, RED_SWORDSMAN_UP
        };
        
        enum Fonts : int {INFO};
        static ResourceManager<sf::Texture, int> textures;
        static ResourceManager<sf::Font, int> fonts;
        static std::unordered_map <int, Animation> animations;
        static Map map;

        static void initialize();

    private:

        static void initTextures();
        static void initAnimations();
        static void initFonts();
        static void initMap();
        static void clear();
};

#endif

